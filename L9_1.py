#################################
### Programming Exercise  2.2 ###
#################################

# Description:
# Implement the following methods:
#  Add a method in myPowerList to read values for a List from a Text file
#    - readFromTextFile(filename)
#
#  Create a class to manage a directory of users containing data
#  Name
#  Address
#  Phone
#  Email
#  The class must be enable:
#    1- Creation of new record
#    2- Save all records in a file
#    3- Load records from a file
#    4 - Search and get data from a given record

# Code:
# First create a python object holding the required structure
import json
import os
import logging
import logging.handlers
import os
import math
import unittest

from random import seed
from random import randint
from pathlib import Path, PureWindowsPath
from types import SimpleNamespace as Namespace
from logging import FileHandler
from logging import Formatter

inputDataTextFileName = 'inputDataTextFile.txt'
#inputDataFolderPath = Path("C:\Users\jvalera\Desktop\maestria\2do semestre\Analisis Diseño y Construcción de Software\Lab 2.2 - Python Practice")
inputDataFolderPath = Path("C:/Users/jvalera/Desktop/maestria/2do semestre/Analisis Diseño y Construcción de Software/Lab 2.2 - Python Practice")
LOG_FORMAT = (
    "%(asctime)s [%(levelname)s]: %(message)s")
MESSAGING_LOG_FILE = "Lab9_1.log"

# Create console logger
logger_console = logging.getLogger("lab9.consoleLogger")
logger_console.setLevel(logging.INFO)
logger_console_handler = logging.StreamHandler()
logger_console_handler.setLevel(logging.INFO)
logger_console_handler.setFormatter(Formatter(LOG_FORMAT))
logger_console.addHandler(logger_console_handler)

# Create file logger
logger_file    = logging.getLogger("lab9.fileLogger")
logger_file.setLevel(logging.INFO)
logger_file_handler = logging.FileHandler(MESSAGING_LOG_FILE)
logger_file_handler.setLevel(logging.INFO)
logger_file_handler.setFormatter(Formatter(LOG_FORMAT))
logger_file.addHandler(logger_file_handler)


def getAllRecords(folder, fileName):
    logger_console.info("Calling getAllRecords")
    logger_file.info("Calling getAllRecords")
    data = '{"default": "null"}'
    data_json = json.loads(data)
    try:
        fileToOpen = folder / fileName
        path_on_windows = PureWindowsPath(fileToOpen)
        with open(path_on_windows, "r") as read_file:
            data_json = json.load(read_file)
    except ValueError as ex:
        errorMessage = "ValueError - Directory file does not contain a valid JSON" 
        logger_console.error(errorMessage)
        logger_file.error(errorMessage)
    except FileNotFoundError:
        errorMessage = "FileNotFoundError - No such file or directory" 
        logger_console.error(errorMessage)
        logger_file.error(errorMessage)
        exit(0)
    return data_json

def searchByKeyAndValue(inputDataFolderPath, inputDataTextFileName, searchKey, searchValue):
    logger_console.info("Calling searchByKeyAndValue")
    logger_file.info("Calling searchByKeyAndValue")
    jdata = json.loads(json.dumps(getAllRecords(inputDataFolderPath, inputDataTextFileName)))
    recordCnt = 0

    message = "Matched records:"
    logger_console.info(message)
    logger_file.info(message)
    
    for result in jdata['arrayOfObjects']:
        if (searchKey in result) and result[searchKey] == searchValue:
            print(result)
            recordCnt = recordCnt + 1
        else:
            message2 = "Not matched " + result
            logger_console.info(message2)
            logger_file.info(message2)

    message3 = "Total records matched" + recordCnt
    logger_console.info(message2)
    logger_file.info(message2)

def writeJsonObjectToFile(folder, fileName, jObj):
    logger_console.info("Calling writeJsonObjectToFile")
    logger_file.info("Calling writeJsonObjectToFile")

    message = "Object to be inserted:\n" + json.dumps(jObj) 

    logger_console.info(message)
    logger_file.info(message)
    
    try:
        fileToOpen = folder / fileName
        path_on_windows = PureWindowsPath(fileToOpen)
        with open(path_on_windows, "w") as write_file:
            write_file.write(json.dumps(jObj))
    except ValueError as ex:
        errorMessage = "ValueError - Invalid data to be written" 
        logger_console.error(errorMessage)
        logger_file.error(errorMessage)
    except FileNotFoundError:
        errorMessage = "FileNotFound - No such file or directory" 
        logger_console.error(errorMessage)
        logger_file.error(errorMessage)
        exit(0)

def addNewRecord(folder, fileName):
    logger_console.info("Calling addNewRecord")
    logger_file.info("Calling addNewRecord")
    jObjArray = {
        "arrayOfObjects" : []
    }
    jObj = {
        "name": "",
        "email": "",
        "age": -1,
        "country": ""
    }

    print
    'Insert new record: \n'
    name_txt = input("Introduce a name\n")
    email_txt = input("Introduce an email\n")
    age_txt = input("Introduce an age\n")
    country_txt = input("Introduce a country\n")

    try:
        age = int(age_txt)
    except ValueError as ex:
        errorMessage = "ValueError please introduce a valid age" 
        logger_console.error(errorMessage)
        logger_file.error(errorMessage)

    jObj['name'] = name_txt
    jObj['email'] = email_txt
    jObj['age'] = age_txt
    jObj['country'] = country_txt

    jsonInFile = getAllRecords(inputDataFolderPath, inputDataTextFileName)
    try:
        if (jsonInFile['default'] == 'null'):
            jObjArray['arrayOfObjects'] = jObj
            entry = jObjArray
    except KeyError:
        jObjArray['arrayOfObjects'].append(jsonInFile['arrayOfObjects'])
        jObjArray['arrayOfObjects'].append(jObj)
        entry = jObjArray

    writeJsonObjectToFile(folder, fileName, entry)

def printMenu_Lab_2_2():
    option = 0
    logger_console.info("Calling printMenu_Lab_2_2")
    logger_file.info("Calling printMenu_Lab_2_2")
    option_txt = input(
        "Select an option:\n1) Add new record\n2) Search record by email\n3) Search record by age\n4) List all records\n")
    try:
        option = int(option_txt)
    except ValueError as ex:
        errorMessage = "ValueError please select a valid option" 
        logger_console.error(errorMessage)
        logger_file.error(errorMessage)
        exit(0)

    if (option < 1 or option > 4):
        errorMessage = "Invalid option" 
        logger_console.error(errorMessage)
        logger_file.error(errorMessage)
    else:
        if (option == 1):
            addNewRecord(inputDataFolderPath, inputDataTextFileName)
        elif (option == 2):
            searchValue = input('Search by email, introduce an email: ')
            searchByKeyAndValue(inputDataFolderPath, inputDataTextFileName, 'email', searchValue)
        elif (option == 3):
            searchValue = input('Search by age, introduce an age: ')
            searchByKeyAndValue(inputDataFolderPath, inputDataTextFileName, 'age', searchValue)
        elif (option == 4):
            logger_console.info("Listing records: ")
            logger_file.info("Listing records: ")

            recordList = getAllRecords(inputDataFolderPath, inputDataTextFileName)

            logger_console.info(recordList)
            logger_file.info(recordList)

#################################
### Programming Exercise  5.1 ###
#################################

# Description:
# Implement the following methods:
# - set_input_data(file_path_name)
#   This methods sets the information about the file that will be used to read the data. Define custom exceptions or error codes for situations where the parameter is incorrect or the file can not be read
#
# - set_output_data(file_path_name)
#   This methods sets the information about the file that will be used to store the sorted data. Define custom exceptions or error codes for situations where the parameter is incorrect or the file can not be created
#
# - execute_merge_sort()
#   This methods sorts the data contained in the file specified. Define custom exceptions or error codes for situations where there may be special errors

# Code:
def set_input_data(inputDataTestFileName):
    logger_console.info("Calling set_input_data")
    logger_file.info("Calling set_input_data")
    inputData = []
    logger_console.info("Processing " + inputDataTestFileName)
    logger_file.info("Processing " + inputDataTestFileName)
    fileData = ""
    try:
        with open(inputDataTestFileName, "r+") as f:
            fileData = f.readlines()
            for line in fileData:
                allNums_txt = line.strip().split(",")
            for num in allNums_txt:
                inputData.append(int(num))
    except FileNotFoundError:
        logger_console.error("Specified file: " + inputDataTestFileName + " does not exists")
        logger_file.error("Specified file: " + inputDataTestFileName + " does not exists")
    except ValueError:
        logger_console.error("Specified file: " + inputDataTestFileName + "contains invalid data ")
        logger_file.error("Specified file: " + inputDataTestFileName + "contains invalid data ")
    except UnboundLocalError:
        logger_console.error("Specified file: " + inputDataTestFileName + " is empty")
        logger_file.error("Specified file: " + inputDataTestFileName + " is empty")
    except Exception as ex:
        template = "set_input_data - An unhandled exception of type {0} occurred. Arguments:\n{1!r}"
        message = template.format(type(ex).__name__, ex.args)
        logger_console.error(message)
        logger_file.error(message)
    print("Input Array :", inputData)
    return inputData


def set_output_data(data, destFileName):
    logger_console.info("Calling set_output_data")
    logger_file.info("Calling set_output_data")
    print("output array:", data)
    try:
        f = open(destFileName, 'w')
        f.write("Sorted data result: \n" + str(data))
        f.close()
    except Exception as ex:
        template = "set_output_data - An unhandled exception of type {0} occurred. Arguments:\n{1!r}"
        message = template.format(type(ex).__name__, ex.args)
        logger_console.error(message)
        logger_file.error(message)

def execute_merge_sort(arr):
    logger_console.info("Calling execute_merge_sort")
    logger_file.info("Calling execute_merge_sort")
    
    arrLen = int(len(arr))
    sortedArr = []
    firstHalfArr = []
    firstHalfArrLen = 0
    secondHalfArr = []
    secondHalfArrLen = 0

    if arrLen > 1:
        middle = int(arrLen / 2)
        firstHalfArr = execute_merge_sort(arr[0:middle])
        firstHalfArrLen = len(firstHalfArr)
        secondHalfArr = execute_merge_sort(arr[middle:arrLen])
        secondHalfArrLen = len(secondHalfArr)
    else:
        return arr

    firstHalfArrIdx = 0
    secondHalfArrIdx = 0
    sortedElements = 0

    while (sortedElements != arrLen):
        firstHalfVal = math.inf
        secondHalfVal = math.inf

        # Sanity check to ensure a valid array access
        if (firstHalfArrIdx <= (firstHalfArrLen - 1)):
            firstHalfVal = firstHalfArr[firstHalfArrIdx]

        # Sanity check to ensure a valid array access
        if (secondHalfArrIdx <= (secondHalfArrLen - 1)):
            secondHalfVal = secondHalfArr[secondHalfArrIdx]

        # Now find the smallest integer and append it to sortedArr
        if (firstHalfVal < secondHalfVal):
            sortedArr.append(firstHalfVal)
            firstHalfArrIdx = firstHalfArrIdx + 1
        else:
            sortedArr.append(secondHalfVal)
            secondHalfArrIdx = secondHalfArrIdx + 1

        sortedElements = sortedElements + 1
    return sortedArr


def generateRandomCSVSetAndWriteToFile(numberOfElements, outputFile):
    logger_console.info("Calling generateRandomCSVSetAndWriteToLog")
    logger_file.info("Calling generateRandomCSVSetAndWriteToLog")
    try:
        f = open(outputFile, 'w')
        seed(1)
        for i in range(numberOfElements):
            randIntVal = randint(0, 100000)

            if i == numberOfElements - 1:
                f.write(str(randIntVal))
            else:
                f.write(str(randIntVal) + ", ")
        f.close()
    except Exception as ex:
        template = "generateRandomCSVSetAndWriteToFile - An unhandled exception of type {0} occurred. Arguments:\n{1!r}"
        message = template.format(type(ex).__name__, ex.args)
        logger_console.error(message)
        logger_file.error(message)
        
# Main program to run used to produce a result
def executeProgram(dataSetName, resultFileName):
    set_output_data(execute_merge_sort(set_input_data(dataSetName)), resultFileName)

while True:
    # Lab 2.2
    dataSetFileName = "dataSet1.txt"
    generateRandomCSVSetAndWriteToFile(2000, dataSetFileName)
    executeProgram(dataSetFileName, "resultFile1.txt")
    # Lab 5.1
    printMenu_Lab_2_2()

