#############################
### Programing Exercise 4 ###
#############################

# Description
#Write a function that computes the standard deviation for a set of numbers coming from a list. Do not use any math module, compute the algorithm

# Testcase
# ds1.ds, ds2.ds

# Code
import math

allNums_txt = []
allNums_int = []
testFile = ["ds1.ds", "ds2.ds"]

def getMean(allNums_int):
  sum = 0
  mean = 0

  for num in allNums_int:
    sum += num

  mean = sum/len(allNums_int)
#  print 'Mean: %s' % (mean)

  return mean

def getStandardDeviation(allNums_int, mean):
  dev = 0

  for num in allNums_int:
    dev += (num - mean)**2

  return math.sqrt(dev/(len(allNums_int) - 1))

for testF in testFile:
  with open(testF, "r+") as f:
    data = f.readlines()
    for line in data:
      allNums_txt += line.strip().split(" ")

    for num in allNums_txt:
      allNums_int.append(int(num))

    #print allNums_int

    print '%s - Standard Deviation: %s' % (testF, getStandardDeviation(allNums_int, getMean(allNums_int)))


