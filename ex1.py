#############################
### Programing Exercise 1 ###
#############################

# Description
#Ask the user for a number. Depending on whether the number is even or odd, print out an appropriate message to the user. Hint: how does an even / odd number react differently when divided by 2?
#If the number is a multiple of 4, print out a different message.

# Testcases
# 1, 5, 4, 8, 2500, 45000, -1, p, t, 45658345213

# Code
value = raw_input("Enter a number: ")
try:
  num = int(value)

  # Number is multiple of 4
  if num % 4 == 0:
    print("The number: ", num, "is multiple of 4")
  # Even number
  elif num % 2:
    print("Odd number: ", num)
  # Odd number
  else:
    print("Even number: ", num)
except ValueError as ex:
  print '"%s" cannot be converted to an int: %s' % (value, ex)

#Ask the user for two numbers: one number to check (call it num) and one number to divide by (check). If check divides evenly into num, tell that to the user. If not, print a different appropriate message.
print("\nNow Please enter 2 numbers: ")

try:
  num   = int(raw_input("Enter first number: "))
except ValueError as ex:
  print '"%s" cannot be converted into an int: %s' % (num, ex)

try:
  check = int(raw_input("Enter second number: "))
except ValueError as ex:
  print '"%s" cannont be converted into an int: %s' % (num, ex)

if ((num < check) or (num % check != 0)):
  print '%s" DOES NOT divide evently into "%s"' % (num, check)
else:
  print '"%s" divides evently into "%s"' % (num, check)


