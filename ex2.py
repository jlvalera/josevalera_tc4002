#############################
### Programing Exercise 2 ###
#############################

# Description
#Generate a random number between 1 and 9 (including 1 and 9). Ask the user to guess the number, then tell them whether they guessed too low, too high, or exactly right. (Hint: remember to use the user input lessons from the very first exercise)
#Keep the game going until the user types "exit"
#Keep track of how many guesses the user has taken, and when the game ends, print this out.

# Testcases
# Record three runs

# Code
# generate random integer values
from random import seed
from random import randint


seed(1)
guesses = 0

while True:
  value = randint(1, 9)
  guess_txt = raw_input("Guess the number: ")

  print("The random number is: ", value)
  if guess_txt == 'exit':
    break

  try:
    guess   = int(guess_txt)
  except ValueError as ex:
    print '"%s" cannot be converted into an int: %s' % (num, ex)

  if guess > value:
    print 'Guess too high'
  elif guess < value:
    print 'Guess to low'
  else:
    print 'Guess exactly right'
    

  guesses = guesses + 1

print("Number of guesses the user has taken: ", guesses)
