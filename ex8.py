#############################
### Programing Exercise 8 ###
#############################

# Description
#Write a module containing different function that computes the
# 1. Sample mean
# 2. Sample standard deviation
# 3. Median
# 4. A function that returns the n-quartil
# 5. A function that returns the n-percentil

# Code
import math

allNums_txt = []
allNums_int = []
testFile = ["ds1.ds", "ds2.ds"]

def getMean(allNums_int):
  sum = 0
  mean = 0

  for num in allNums_int:
    sum += num

  mean = sum/len(allNums_int)
#  print 'Mean: %s' % (mean)

  return mean

def getStandardDeviation(allNums_int, mean):
  dev = 0

  for num in allNums_int:
    dev += (num - mean)**2

  return math.sqrt(dev/(len(allNums_int) - 1))

def getMedian(allNums_int):
  retVal = 0
  meanIdx = len(allNums_int)/2

  if(meanIdx % 1 == 0):
    retVal = allNums_int[meanIdx]
    #print 'debug median allNums_int[%s] = %s' % (meanIdx, allNums_int[meanIdx])
  else:
    retVal = (allNums_int[math.floor(meanIdx)] + allNums_int[math.ceil(meanIdx)])/2
    #print 'debug median (allNums_int[%s] - allNums_int[s])/2 ' % (math.floor(meanIdx), math.ceil(meanIdx))

  return retVal

def getPercentile(allNums_int, pNum):
  input_num = -1

  if pNum == None:
    input_txt = raw_input("Enter the desired percentile: " )
    try:
      input_num = int(input_txt)
    except ValueError as ex:
      print '"%s" cannot be converted into an int: %s' % (num, ex)
  else:
    input_num = pNum

  allNums_int.sort()

  idx = int(round((len(allNums_int) - 1) * (input_num/100.0), 0))
  #print 'idx = %s, len(allNums_int) = %s' % (idx, len(allNums_int) - 1)
  #print 'idx = %s, num = %s, percentile = %s' % (idx, input_num, allNums_int[idx])

  
  return int(allNums_int[idx])

for testF in testFile:
  with open(testF, "r+") as f:
    data = f.readlines()
    for line in data:
      allNums_txt += line.strip().split(" ")

    for num in allNums_txt:
      allNums_int.append(int(num))

    # First sort all the numbers
    allNums_int.sort()
    print '%s - Sample mean: %s' % (testF, getMean(allNums_int))
    print '%s - Standard Deviation: %s' % (testF, getStandardDeviation(allNums_int, getMean(allNums_int)))
    print '%s - Median %s' % (testF, getMedian(allNums_int))
    print '%s - 1 quartil %s' % (testF, getPercentile(allNums_int,25))
    print '%s - 2 quartil %s' % (testF, getPercentile(allNums_int,50))
    print '%s - 3 quartil %s' % (testF, getPercentile(allNums_int,75))
    print '%s - 4 quartil %s' % (testF, getPercentile(allNums_int,100))
    print '%s - npercentile %s' % (testF, getPercentile(allNums_int,None))
