#############################
### Programing Exercise 3 ###
#############################

# Description
#Write a program (function!) that takes a list and returns a new list that contains all the elements of the first list minus all the duplicates.
#Write two different functions to do this - one using a loop and constructing a list, and another using sets.

# Testcase
# [1,1,2,3,4,5,1,5,7,7,8,9,0,0,0,0]
# [a,1,b,0,b,1,e,r,t,p,q]
# []
# Add more test cases for large lists

# Code
def printList(nList):
  print '[',
  for x in range(len(nList)):
    print '%s' % nList[x],
  print ']'

def constructList():
  nList = []
  print "Create a new list\nCurrent list []"
  
  while True:

    input_txt = raw_input("Enter number element: ")

    if input_txt == 'exit':
      break

    try:
      listElem = int(input_txt)
    except ValueError as ex:
      print '"%s" cannot be converted into an int: %s' % (num, ex)
      quit()
    nList.append(listElem)
    printList(nList)

  return nList    

  
def removeDuplicates(nList):
  retSet = set()

  for x in range(len(nList)):
    retSet.add(nList[x])

  return list(retSet)
  

def main():
  uniqueList = removeDuplicates(constructList())
  printList(uniqueList) 

main()
