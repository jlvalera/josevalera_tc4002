#############################
### Programing Exercise 5 ###
#############################

# Description
# Write a function that receives as parameters how many Fibonnaci numbers to generate and then generates them. Take this opportunity to think about how you can use functions. Make sure to ask the user to enter the number of numbers in the sequence to generate.(Hint: The Fibonnaci sequence is a sequence of numbers where the next number in the sequence is the sum of the previous two numbers in the sequence. The sequence looks like this: 1, 1, 2, 3, 5, 8, 13,)

# Testcase
# 0,0.5,1,3,8,2000,450000,-1,p,[]

# Code

def generateFibonnaci(num):
  fib = []

  if (num >= 1):
    fib.append(1)

  if (num >= 2):
    fib.append(1)

  if (num > 2):
    for x in range (2, num):
      fib.append(fib[x-2] + fib[x-1])

  return fib


value = raw_input("Enter a number: ")
num = -1

try:
  num = int(value)

  if (num % 1) != 0:
    print '"%s" cannot be converted into an int' % (num)
    quit()
    
except ValueError as ex:
  print '"%s" cannot be converted into an int: %s' % (num, ex)
 

if num <= 0:
  print 'Invalid number - %s must be a positive integer' % (num)
  quit()

print 'Fibonacci: %s' % generateFibonnaci(num)
