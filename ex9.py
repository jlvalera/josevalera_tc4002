#############################
### Programing Exercise 1 ###
#############################

# Description
# Write a function that converts a decimal number into a Roman format

# Testcases

# Code
def generateRomanNumber(num):
  magnitude = ['I', 'V', 'X', 'X', 'L', 'C', 'C', 'D', 'M']
  magnitude_idx = 0;
  roman_num = []

  while(num != 0):
    temp_num = num % 10
    print 'temp_num: %s, magnitud_idx %s' % (temp_num, magnitude_idx)
    if   (temp_num == 0):
      pass
    elif (temp_num == 1):
      roman_num.insert(0, magnitude[magnitude_idx])
    elif (temp_num == 2):
      roman_num.insert(0, magnitude[magnitude_idx])
      roman_num.insert(0, magnitude[magnitude_idx])
    elif (temp_num == 3):
      roman_num.insert(0, magnitude[magnitude_idx])
      roman_num.insert(0, magnitude[magnitude_idx])
      roman_num.insert(0, magnitude[magnitude_idx])
    elif (temp_num == 4):
      roman_num.insert(0, magnitude[magnitude_idx + 1])
      roman_num.insert(0, magnitude[magnitude_idx])
    elif (temp_num == 5):
      roman_num.insert(0, magnitude[magnitude_idx + 1])
    elif (temp_num == 6):
      roman_num.insert(0, magnitude[magnitude_idx ])
      roman_num.insert(0, magnitude[magnitude_idx + 1])
    elif (temp_num == 7):
      roman_num.insert(0, magnitude[magnitude_idx ])
      roman_num.insert(0, magnitude[magnitude_idx ])
      roman_num.insert(0, magnitude[magnitude_idx + 1])
    elif (temp_num == 8):
      roman_num.insert(0, magnitude[magnitude_idx ])
      roman_num.insert(0, magnitude[magnitude_idx ])
      roman_num.insert(0, magnitude[magnitude_idx ])
      roman_num.insert(0, magnitude[magnitude_idx + 1])
    elif (temp_num == 9):
      roman_num.insert(0, magnitude[magnitude_idx + 3 ])
      roman_num.insert(0, magnitude[magnitude_idx  ])

    num = num / 10
    magnitude_idx = magnitude_idx + 3
    if (num == 0):
      break

  return roman_num

input_txt = raw_input("Which number would you like to convert to roman?: " )
try:
  input_num = int(input_txt)
except ValueError as ex:
  print '"%s" cannot be converted into an int: %s' % (num, ex)

print '%s in roman is: %s' % (input_num, generateRomanNumber(input_num))
